import { render, screen, fireEvent } from "@testing-library/react";
import App from "../src/App";

describe("UI test", () => {

  it("should display the page title", () => {
    // 1. Render App
    render(<App />);

    // 2. Expect that the title is there
    expect(screen.getByRole("heading")).toBeInTheDocument();

    // 3. Expect that the title is : "Teenage Boy Speaking"
    expect(screen.getByRole("heading")).toHaveTextContent(
      "Teenage Boy Speaking"
    );
  });

  it("should have input", () => {
    // 1. Render App
    render(<App />);

    // 2. Expect that input is there
    expect(screen.getByRole("textbox")).toBeInTheDocument();
  });

  it("should have a button submit", () => {
    // 1. Render App
    render(<App />);

    // 2. Expect that a button submit is there
    expect(screen.getByRole("button")).toBeInTheDocument();
  });

  it("should display the response text", () => {
    // 1. Render App
    render(<App />);

    // 2. Expect that the response text is there
    expect(screen.getByText("GGGggggggghhhh !")).toBeInTheDocument();
  });

  it("should update the response text based on the input", () => {
    // 1. Render App
    render(<App />);

    // 2. Get input and button elements
    const input = screen.getByRole("textbox");
    const button = screen.getByRole("button");

    // 3. Simulate user typing a question into the input field
    fireEvent.change(input, { target: { value: "Comment allez-vous ?" } });

    // 4. Simulate form submission
    fireEvent.click(button);

    // 5. Expect the response text to be updated
    expect(screen.getByText("Bien sûr.")).toBeInTheDocument();
  });

  it("should update the response text based on the input", () => {
    // 1. Render App
    render(<App />);

    // 2. Get input and button elements
    const input = screen.getByRole("textbox");
    const button = screen.getByRole("button");

    // 3. Simulate user typing a question into the input field
    fireEvent.change(input, { target: { value: "JE CRIE FORT" } });

    // 4. Simulate form submission
    fireEvent.click(button);

    // 5. Expect the response text to be updated
    expect(screen.getByText("Whoa, calmos !")).toBeInTheDocument();
  });

  it("should update the response text based on the input", () => {
    // 1. Render App
    render(<App />);

    // 2. Get input and button elements
    const input = screen.getByRole("textbox");
    const button = screen.getByRole("button");

    // 3. Simulate user typing a question into the input field
    fireEvent.change(input, { target: { value: "MA QUESTION UPPERCASE ?" } });

    // 4. Simulate form submission
    fireEvent.click(button);

    // 5. Expect the response text to be updated
    expect(screen.getByText("Calmez-vous, je sais ce que je fais !")).toBeInTheDocument();
  });

  it("should update the response text based on the input", () => {
    // 1. Render App
    render(<App />);

    // 2. Get input and button elements
    const input = screen.getByRole("textbox");
    const button = screen.getByRole("button");

    // 3. Simulate user typing a question into the input field
    fireEvent.change(input, { target: { value: "" } });

    // 4. Simulate form submission
    fireEvent.click(button);

    // 5. Expect the response text to be updated
    expect(screen.getByText("Bien, on fait comme ça !")).toBeInTheDocument();
  });

  it("should update the response text based on the input", () => {
    // 1. Render App
    render(<App />);

    // 2. Get input and button elements
    const input = screen.getByRole("textbox");
    const button = screen.getByRole("button");

    // 3. Simulate user typing a question into the input field
    fireEvent.change(input, { target: { value: "heuuuuu" } });

    // 4. Simulate form submission
    fireEvent.click(button);

    // 5. Expect the response text to be updated
    expect(screen.getByText("Peu importe !")).toBeInTheDocument();
  });

  it("should display the response text", () => {
    // 1. Render App
    render(<App />);

    // 2. Expect that the response text is there
    expect(screen.getByText("GGGggggggghhhh !")).toBeInTheDocument();
  });
});
