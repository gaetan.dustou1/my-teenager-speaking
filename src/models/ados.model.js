// ados.model.js
export class BadBoy {
  constructor() {}

  getQcu(question) {
    return question;
  }

  returnResponse(question) {
    const regexNorm = /^[A-Z][a-z\s-]+\s\?/;
		const regexCrier = /^[A-Z\s]+[A-Z]$/;
		const regexInsist = /^[A-Z\s]+\s\?+$/;
		const regexEmpty = /^$/;
		const regexAny = /.*/;

    let response;
		if (regexInsist.test(question)) {
      response = "Calmez-vous, je sais ce que je fais !";
   
    }else if (regexCrier.test(question)) {
			response = "Whoa, calmos !";
			
		}else if(regexNorm.test(question)) {
      response = "Bien sûr.";
			
		}else if(regexEmpty.test(question)) {
      response = "Bien, on fait comme ça !";
      
    }else if(regexAny.test(question)) {
      response = "Peu importe !";
    
    } else {
      response = "GGGggggggghhhh !";
    }
    return response;
  }
}
