import { useState, useRef } from 'react';
import maxresdefault from '../public/img/maxresdefault.jpg';
import { BadBoy } from "../src/models/ados.model";

const badBoy = new BadBoy();

function App() {
  const [responseText, setResponseText] = useState("GGGggggggghhhh !");
  const questionRef = useRef(null);

  const handleSubmit = (event) => {
    event.preventDefault();
    const question = questionRef.current.value;
    console.log("The question:", question);
    const response = badBoy.returnResponse(question);
    setResponseText(response);
  };

  return (
    <>
      <div className='flex justify-center mt-10 mb-10'>
        <h1 className='text-4xl'>Teenage Boy Speaking</h1>
      </div>
      <div className='flex mb-10'>
        <div className="mt-4 w-1/2 flex justify-end">
          <img src={maxresdefault} alt="" className="w-2/5"/>
        </div>
        <div className='flex flex-col justify-center'>
          <div className="mt-5 thought" id="responseTexte">
            {responseText}
          </div>
        </div>
      </div>
      <hr className='m-auto w-2/3 mb-10'/>
      
      <form onSubmit={handleSubmit} className="flex justify-center">
        <div className="form-group">
          <input ref={questionRef} id="question" className="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" type="text"/>
        </div>
        <div className="form-group">
          <input className="bg-sky-500 hover:bg-sky-700 ml-2 px-5 py-1.5 rounded text-white font-bold" type="submit" value="Parler"/>
        </div>
      </form>
    </>
  );
}

export default App;
